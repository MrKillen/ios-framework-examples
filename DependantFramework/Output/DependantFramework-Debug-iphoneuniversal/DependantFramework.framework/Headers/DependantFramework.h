//
//  DependantFramework.h
//  DependantFramework
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DependantFramework.
FOUNDATION_EXPORT double DependantFrameworkVersionNumber;

//! Project version string for DependantFramework.
FOUNDATION_EXPORT const unsigned char DependantFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DependantFramework/PublicHeader.h>
#import <DependantFramework/CatViewController.h>

