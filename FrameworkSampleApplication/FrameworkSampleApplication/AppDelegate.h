//
//  AppDelegate.h
//  FrameworkSampleApplication
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

