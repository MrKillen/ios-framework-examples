//
//  ViewController.m
//  FrameworkSampleApplication
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import "ViewController.h"
@import DynamicFramework.GibbonViewController;
@import StaticFramework.StaticViewController;


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)viewControllerFromDynamic:(id)sender {
    GibbonViewController *viewControllerFromFramework = [GibbonViewController initGibbonViewController];
    
    [self.navigationController pushViewController:viewControllerFromFramework animated:YES];
    
}

- (IBAction)viewControllerFromStatic:(id)sender {
    StaticViewController *staticViewController = [StaticViewController initStaticViewController];    
    [self.navigationController pushViewController:staticViewController animated:YES];
}


@end
