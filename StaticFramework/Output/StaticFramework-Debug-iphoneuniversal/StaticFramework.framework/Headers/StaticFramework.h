//
//  StaticFramework.h
//  StaticFramework
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for StaticFramework.
FOUNDATION_EXPORT double StaticFrameworkVersionNumber;

//! Project version string for StaticFramework.
FOUNDATION_EXPORT const unsigned char StaticFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StaticFramework/PublicHeader.h>
#import <StaticFramework/StaticViewController.h>


