//
//  StaticViewController.h
//  StaticFramework
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StaticViewController : UIViewController

+(instancetype) initStaticViewController;

@end
