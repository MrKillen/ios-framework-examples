//
//  StaticViewController.m
//  StaticFramework
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import "StaticViewController.h"

@interface StaticViewController ()

@end

@implementation StaticViewController


+(instancetype) initStaticViewController {
    NSString *storyBoardName = @"Storyboard";
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSBundle *staticBundle = [[NSBundle alloc] initWithPath:[mainBundle pathForResource:@"StaticFrameworkBundle" ofType:@"bundle"]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyBoardName bundle:staticBundle];
    
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)callAction:(id)sender {
    NSString *message = @"This is Called from the static Framework Baby!";
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:okAction];

    [self presentViewController:alert animated:NO completion:^{

    }];
}


@end
