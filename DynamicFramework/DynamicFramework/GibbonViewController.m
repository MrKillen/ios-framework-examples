//
//  GibbonViewController.m
//  DynamicFramework
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import "GibbonViewController.h"
@import DependantFramework;

@interface GibbonViewController ()

@end

@implementation GibbonViewController



+(instancetype) initGibbonViewController {
    NSString *frameworkBundleIdentifer = @"com.cafex.DynamicFramework";
    NSString *storyBoardName = @"Storyboard";
    
    NSBundle *bundle = [NSBundle bundleWithIdentifier:frameworkBundleIdentifer];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyBoardName bundle:bundle];
    
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];
}




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"DynamicFramework";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)viewControllerFromDynamicFramework:(id)sender {
    if([CatViewController class]) {
        CatViewController *viewControllerFromFramework = [CatViewController initCatViewController];
        [self.navigationController pushViewController:viewControllerFromFramework animated:YES];
    }
    else {
        [self alertUserThatDependantFrameworkIsNotEmbedded];
    }
}

-(void) alertUserThatDependantFrameworkIsNotEmbedded {
    NSString *message = @"Dependant framework is not included in the project";
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"warning" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
                                
                                    

@end
