//
//  DynamicFramework.h
//  DynamicFramework
//
//  Created by pkillen on 15/01/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DynamicFramework.
FOUNDATION_EXPORT double DynamicFrameworkVersionNumber;

//! Project version string for DynamicFramework.
FOUNDATION_EXPORT const unsigned char DynamicFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DynamicFramework/PublicHeader.h>
#import <DynamicFramework/GibbonViewController.h>


