# README #


### What is this repository for? ###

This Repo is just a general overview on how to use dynamic and static frameworks in iOS

### How do I get set up? ###

* Open FrameworkExamples.xcworkspace
* Connect to a iOS device
* Build the FrameworkSampleApplication.

### How do I create a dynamic framework in xCode? ###

* The default template will create a dynamic library

![Alt text](http://gdurl.com/U8gMT)

### How do I make my classes public ? ###

* Add your imports to your framework here
```objective-c
#import <UIKit/UIKit.h>

//! Project version number for DynamicFramework.
FOUNDATION_EXPORT double DynamicFrameworkVersionNumber;

//! Project version string for DynamicFramework.
FOUNDATION_EXPORT const unsigned char DynamicFrameworkVersionString[];

// Add your imports here!
#import <DynamicFramework/GibbonViewController.h> 
```

* Make your headers public in the Build phase

![Alt text](http://gdurl.com/yVxV)


### How do I use the framework in my application ? ###

* Embed the framework into the application and Link it

![Alt text](http://gdurl.com/rTJD)

### How do I get my resources from my framework ? ###

* Use the identifier of your framework to get the bundle 

```objective-c
    NSString *frameworkIdentifer = @"com.cafex.DynamicFramework";
    NSString *storyBoardName = @"Storyboard";
    
    NSBundle *bundle = [NSBundle bundleWithIdentifier:frameworkIdentifer];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyBoardName bundle:bundle];
```

### Can I use a dynamic framework in a dynamic framework? ###

* Yes , you need to add it to the Link Binary With Libraries in Build Phases

### Why I am getting Reason: image not found when trying to run a dynamic framework ###

* Make sure to embed all of your dynamic frameworks in the application.

### How do I create a static framework?  ###

* Use the default framework template

![Alt text](http://gdurl.com/U8gMT)

* Change the Mach - O Type to Static in Target->Build Settings.

### How do I use bundle resources in a static framework?  ###

* Add your resources to a bundle
* Add the Bundle file to your application.
* Load your Bundle from the main bundle to access your assets

```objective-c
    NSString *storyBoardName = @"Storyboard";
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSBundle *staticBundle = [[NSBundle alloc] initWithPath:[mainBundle pathForResource:@"StaticFrameworkBundle" ofType:@"bundle"]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyBoardName bundle:staticBundle];
    
    return [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([self class])];

```

### Handy Internet Links ###

[ Apple talk about frameworks ](https://developer.apple.com/videos/play/wwdc2014-416/)
[Handy information about creating a bundle for ios](http://www.raywenderlich.com/65964/create-a-framework-for-ios)

[ Apple talk about frameworks ](https://developer.apple.com/videos/play/wwdc2014-416/)

[ Good Blog post about why static frameworks are bad](http://landonf.bikemonkey.org/code/ios/Radar_15800975_iOS_Frameworks.20140112.html)

[Overview of dynamic frameworks from Apple](https://developer.apple.com/library/mac/documentation/DeveloperTools/Conceptual/DynamicLibraries/100-Articles/OverviewOfDynamicLibraries.html)